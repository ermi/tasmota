#!/usr/bin/python3

#  tasmota_tele.py: script to handle tasmota plug adapters
#  Copyright (C) 2022  ermi
#
#  This code is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import datetime as dt
import paho.mqtt.client
import tasmota
import json
import sys
import time

class evaluate_plot(object):
	def __init__(self):
		self.data = []
		self.length = 0
	
	def on_message(self, client, userdata, message):
		data = json.loads(message.payload)
		self.data.append(data)
	
	def sleep(self, timeout):
		if len(self.data)!=self.length:
			self.length = len(self.data)
			x = [dt.datetime.fromisoformat(d['Time']) for d in self.data]
			y = [d['ENERGY']['Power'] for d in self.data]
			
			plt.clf()
			plt.plot(x, y, '-x')
			plt.grid(which='both')
			plt.ylabel('Power in W')
			plt.pause(timeout)
		else:
			time.sleep(timeout)


class evaluate_print(object):
	def __init__(self):
		self.c = '['
	def __del__(self):
		print('\n]')
	
	def on_message(self, client, userdata, message):
		data = json.loads(message.payload)
		print(self.c, end='\n\t')
		self.c = ','
		print(data, end='', flush=True)
	
	def sleep(self, timeout):
		time.sleep(timeout)


def usage():
	print('usage:', file=sys.stderr)
	print(' ', sys.argv[0], '<JSON-CONFIG> <print|plot>', file=sys.stderr)

if __name__=='__main__':
	if len(sys.argv)!=3:
		usage()
		sys.exit(1)
	
	with open(sys.argv[1]) as f:
		cnf = json.load(f)
	
	client = paho.mqtt.client.Client(cnf['broker'])
	client.username_pw_set(cnf['user'], cnf['password'])
	client.connect(cnf['broker'], cnf.get('port', 1883))
	
	if sys.argv[2]=='print':
		ev = evaluate_print()
	elif sys.argv[2]=='plot':
		ev = evaluate_plot()
	else:
		print('Error: command not supported', file=sys.stderr)
		usage()
		sys.exit(1)
	
	obj = tasmota.mqtt(client, cnf['topic'])
	obj.tele_cb = ev.on_message
	obj.subscribe_tele('SENSOR')
	obj.loop_start()
	obj.teleperiod(10)
#	obj.loop_forever()
	try:
		while True:
			ev.sleep(1)
	except KeyboardInterrupt:
		obj.loop_stop()

