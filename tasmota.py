#!/usr/bin/python3

#  tasmota.py: script to handle tasmota plug adapters
#  Copyright (C) 2022  ermi
#
#  This code is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import paho.mqtt.client
import requests
import queue
import json
import sys


class __tasmota_commands__(object):
	def teleperiod(self, value=None):
		return self.command('Teleperiod', value)
	
	def sensor(self):
		return self.command('Status', 10)
	def state(self):
		return self.command('STATE')
	
	def power(self, value=None):
		return self.command('Power', value)
	def power_on(self):
		return self.command('Power', 'ON')
	def power_off(self):
		return self.command('Power', 'OFF')
	def power_toggle(self):
		return self.command('Power', 'TOGGLE')


class http(__tasmota_commands__):
	def __init__(self, host):
		self.host = host
	
	def command(self, key, value=None):
		url = 'http://%s/cm?cmnd=%s'%(self.host, key)
		if value:
			url += ' '+str(value)
		return requests.get(url).json()


class mqtt(__tasmota_commands__):
	def __init__(self, mqtt_client, topic_name):
		self.topic_name = topic_name
		self.client = mqtt_client
		self.client.on_message = self.on_message
		self.client.subscribe('stat/%s/RESULT'%(self.topic_name))
		self.client.subscribe('stat/%s/STATUS10'%(self.topic_name))
		self.queue = queue.Queue()
	
	def loop_start(self):
		self.client.loop_start()
	def loop_stop(self):
		self.client.loop_stop()
	def loop_forever(self):
		self.client.loop_forever()
	
	def subscribe_tele(self, key):
		self.client.subscribe('tele/%s/%s'%(self.topic_name, key))
	
	def on_message(self, client, userdata, message):
		if message.topic.startswith('tele/'):
			self.tele_cb(client, userdata, message)
		else:
			self.queue.put(message)
	
	def command(self, key, value=None, timeout=None):
		self.client.publish('cmnd/%s/%s'%(self.topic_name, key), value)
		message = self.queue.get(timeout=timeout)
		return message.payload.decode()


########
# main #
########
def usage():
	print('usage:', file=sys.stderr)
	print(' ', sys.argv[0], 'http <HOST> [COMMAND]', file=sys.stderr)
	print(' ', sys.argv[0], 'mqtt <JSON-CONFIG> [COMMAND]', file=sys.stderr)
	print(file=sys.stderr)
	print('COMMAND:', file=sys.stderr)
	print('  power[=<ON|OFF|TOGGLE>]', file=sys.stderr)
	print('  state', file=sys.stderr)
	print('  sensor', file=sys.stderr)

def main(obj, cmds):
	for cmd in cmds:
		lst = cmd.split('=')
		if len(lst)==2 and (lst[0] in ('power', )):
			print(getattr(obj, lst[0])(lst[1]))
		elif len(lst)==1 and (lst[0] in ('power', 'state', 'sensor')):
			print(getattr(obj, lst[0])())
		else:
			print('Error: command not supported (%s)'% cmd, file=sys.stderr)
			sys.exit(1)

if __name__=='__main__':
	if len(sys.argv)<3:
		usage()
		sys.exit(1)
	
	if sys.argv[1]=='http':
		tasmota_host = sys.argv[2]
		cmd = sys.argv[3:]
		
		obj = http(tasmota_host)
		main(obj, cmd)
	elif sys.argv[1]=='mqtt':
		with open(sys.argv[2]) as f:
			cnf = json.load(f)
		cmd = sys.argv[3:]
		
		client = paho.mqtt.client.Client(cnf['broker'])
		client.username_pw_set(cnf['user'], cnf['password'])
		client.connect(cnf['broker'], cnf.get('port', 1883))
		
		obj = mqtt(client, cnf['topic'])
		obj.loop_start()
		main(obj, cmd)
		obj.loop_stop()
	else:
		print('Error: protocol not supported', file=sys.stderr)
		usage()
		sys.exit(1)

